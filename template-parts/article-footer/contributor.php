<?php if(get_field('one_time_contributor')): ?>

	<section id="contributor" class="one-time">

		<div class="info">
			<h5>Contributor</h5>
			<h3><?php echo get_field('contributor_name'); ?></h3>
			<?php if(get_field('contributor_bio')): ?>
				<?php echo get_field('contributor_bio'); ?>
			<?php endif; ?>
		</div>

	</section>

<?php else: ?>

	<?php $posts = get_field('contributor'); if( $posts ): ?>

	    <?php foreach( $posts as $post): setup_postdata($post); ?>

			<section id="contributor">
				<div class="photo">
					<img src="<?php $image = get_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</div>
				<div class="info">
					<h5><?php echo get_field('title'); ?></h5>
					<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
					<?php echo get_field('short_bio'); ?>

					<div class="links">
						<a href="<?php the_permalink(); ?>">Read full bio</a>
						<a href="<?php the_permalink(); ?>#posts">More posts by <?php the_title(); ?></a>
					</div>
				</div>

			</section>
			
	    <?php endforeach; ?>

	<?php wp_reset_postdata(); endif; ?>


<?php endif; ?>