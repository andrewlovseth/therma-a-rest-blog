<section id="share">
	<h5>Share this post</h5>

	<div class="links-wrapper">
		<div class="facebook-link share-link">
			<a tabindex="0" role="button" aria-label="Share this story to Facebook" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode(get_permalink()); ?>" class="facebook" rel="external">
				<?php get_template_part('svg/icon-facebook'); ?>
			</a>
		</div>

		<div class="twitter-link share-link">
			<a tabindex="0" role="button" aria-label="Share this story to Twitter" href="https://twitter.com/intent/tweet/?text=<?php echo urlencode(get_the_title()); ?>&url=<?php echo urlencode(get_permalink()); ?>" class="twitter" rel="external">
				<?php get_template_part('svg/icon-twitter'); ?>
			</a>
		</div>

		<div class="page-link share-link">
			<a href="#" tabindex="0" role="button" aria-label="Copy the link to this story" class="link-trigger" data-link="<?php echo get_permalink(); ?>">
				<?php get_template_part('svg/icon-link'); ?>
			</a>

			<span class="page-link-view"></span>
		</div>
	</div>

</section>