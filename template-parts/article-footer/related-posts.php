<section id="posts" class="related">
	<div class="wrapper">

		<h2 class="section-header">Related Posts</h2>

		<div class="posts-wrapper">

			<?php

				$posttags = get_the_tags();
				if ($posttags) {
					foreach($posttags as $tag) {
						$tagIn[] = $tag->term_id;
					}
				}
				$exclude[] = get_the_ID();

				$args = array(
					'post_type' => array('post'),
					'posts_per_page' => 12,
					'pagination' => true,
					'paged' => $paged,
					'tag__in' => $tagIn,
					'post__not_in' => $exclude
				);

				$relatedPosts = new WP_Query( $args );
				if ( $relatedPosts->have_posts() ) : ?>

					<?php while ( $relatedPosts->have_posts() ) : $relatedPosts->the_post(); ?>

						<article class="related">
							<?php get_template_part('template-parts/global/article-profile'); ?>
						</article>

					<?php endwhile; ?>

			<?php endif; wp_reset_postdata(); ?>

		</div>


	</div>
</section>