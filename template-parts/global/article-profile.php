	<a href="<?php the_permalink(); ?>" class="photo">
		<div class="content">
			<img src="<?php $image = get_field('featured_image'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
		</div>
	</a>

	<div class="info">
		<h4 class="category"><?php the_category( ' ' ); ?></h4>
		<h2 class="title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
		<h5><?php comments_number( 'O Comments', '1 Comment', '% Comments' ); ?></h5>
	</div>