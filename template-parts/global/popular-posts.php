<section id="popular">
	<div class="wrapper">

		<h2 class="section-header">Most Popular</h2>

		<div class="posts-wrapper">

			<?php
				$args = array(
				    'post_html' => '
				    	<li>
					    <article class="profile">
						    <div class="photo cover" style="background-image: url();">{thumb_img}</div>
						    <div class="info">
						    	<h4 class="category">{category}</h4>
						    	<h2 class="title"><a href="{url}">{text_title}</a></h2>
						    	<h5>{comments} comments</h5>
						    </div>
					    </article>
					    </li>
				    ',
				    'stats_comments' => 1,
				    'stats_category' => 1,
				    'thumbnail_width' => 160,
				    'thumbnail_height' => 160
				);

				wpp_get_mostpopular( $args );
			?>

		</div>

	</div>
</section>