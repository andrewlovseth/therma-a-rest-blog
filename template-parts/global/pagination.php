<section id="pagination">
    <?php echo get_the_posts_pagination( array(
        'mid_size' => 3,
        'prev_text' => '&lt;',
        'next_text' => '&gt;',
        'aria_label' => 'Posts'
    ) ); ?>
</section>