	<a href="<?php the_permalink(); ?>" class="photo">
		<div class="content">
			<img src="<?php $image = get_field('featured_image'); echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>" />			
		</div>
	</a>

	<div class="info">
		<h4 class="category"><?php the_category( ' ' ); ?></h4>
		<h2 class="title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
		<h3 class="byline">
			<?php if(get_field('one_time_contributor') == true): ?>

				<?php echo get_field('contributor_name'); ?> | 

			<?php else: ?>

				<?php $contributors = get_field('contributor'); if( $contributors ): ?>

					<?php foreach( $contributors as $contributor): ?>
						<a class="contributor" href="<?php echo get_permalink($contributor->ID); ?>"><?php echo get_the_title($contributor->ID); ?></a> | 
					<?php endforeach; ?>

				<?php endif; ?>

				
			<?php endif; ?>

			<?php the_time('F j, Y'); ?>
		</h3>

		<?php the_excerpt(); ?>

		<a href="<?php the_permalink(); ?>" class="more" aria-label="Read the article: <?php the_title(); ?>">More</a>
	</div>