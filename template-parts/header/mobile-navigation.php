<nav class="mobile-navigation">
    <div class="mobile-navigation-container">

    </div>

    <div class="screens-container">
        <?php if(have_rows('mobile_navigation', 'options')): while(have_rows('mobile_navigation', 'options')) : the_row(); ?>

            <?php if( get_row_layout() == 'screen' ): ?>

                <ul data-id="<?php $id = get_sub_field('id'); echo $id; ?>">
                    <?php if(have_rows('links')): while(have_rows('links')): the_row(); ?>
                        
                        <?php 
                            $type = get_sub_field('type');
                            $target = get_sub_field('target');
                            $link = get_sub_field('link');
                            if( $link ): 
                            $link_url = $link['url'];
                            $link_title = $link['title'];
                            $link_target = $link['target'] ? $link['target'] : '_self';
                        ?>

                            <li class="<?php echo $type; ?>">
                                <a href="<?php echo esc_url($link_url); ?>" data-target="<?php echo $target; ?>">
                                    <?php echo esc_html($link_title); ?>
                                </a>
                            </li>

                        <?php endif; ?>

                    <?php endwhile; endif; ?>
                    
                </ul>

            <?php endif; ?>

        <?php endwhile; endif; ?>

    </div>
</nav>