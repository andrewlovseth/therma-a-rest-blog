<?php

	$logo = get_field('blog_logo', 'options');

?>

<nav class="blog-nav">

	<div class="mobile-blog-logo">
		<a href="<?php echo site_url('/'); ?>" aria-label="Therm-a-Rest Dream Team Blog Logo">
			<?php echo wp_get_attachment_image($logo['ID'], 'full'); ?>	
		</a>
	</div>

	<div class="blog-nav-header">
		<div class="mobile-toggle">
			<a href="#" class="blog-nav-trigger">
				<span class="label">Categories</span>
				<img src="<?php bloginfo('template_directory'); ?>/images/icon-dropdown-arrow.svg" alt="Menu Toggle" />
			</a>
		</div>
	</div>

	<div class="blog-nav-links">

		<div class="blog-logo">
			<a href="<?php echo site_url('/'); ?>" aria-label="Therm-a-Rest Dream Team Blog Logo">
				<?php echo wp_get_attachment_image($logo['ID'], 'full'); ?>	
			</a>
		</div>

		<?php if(have_rows('blog_navigation', 'options')): while(have_rows('blog_navigation', 'options')): the_row(); ?>

			<?php 
				$link = get_sub_field('link');
				if( $link ): 
				$link_url = $link['url'];
				$link_title = $link['title'];
				$link_target = $link['target'] ? $link['target'] : '_self';
			 ?>

			 	<a href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>

			<?php endif; ?>

		<?php endwhile; endif; ?>

		<div class="search-btn closed">
			<a href="#" class="search-trigger search-trigger-open" tabindex="0" role="button" aria-label="Toggle search field">
				<img src="<?php bloginfo('template_directory'); ?>/images/icon-search.svg" alt="Search" />
			</a>

			<a href="#" class="search-trigger search-trigger-closed" tabindex="0" role="button" aria-label="Toggle search field">
				<img src="<?php bloginfo('template_directory'); ?>/images/icon-search.svg" alt="Search" />
			</a>

			<div class="form-wrapper blog-search">
				<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
					<label>
						<span class="screen-reader-text"><?php echo _x( 'Search for:', 'label' ) ?></span>
						<input type="search" class="search-field"
							placeholder="<?php echo esc_attr_x( 'Search …', 'placeholder' ) ?>"
							value="<?php echo get_search_query() ?>" name="s"
							title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" />
					</label>
				</form>
			</div>				
		</div>
	</div>
</nav>