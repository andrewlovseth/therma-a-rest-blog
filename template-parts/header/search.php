<div class="site-search">
    <div class="search-header">
        <div class="logo">
            <a href="https://www.thermarest.com/">
                <img src="https://www.thermarest.com/on/demandware.static/-/Sites-cdi-storefront-catalog-us/default/dw1a2ff2c6/header_tar_logo.svg">
            </a>
        </div>

        <form class="search-form">
            <input type="search" class="search-input" placeholder="Search" />
            <button class="submit fa fa-search cursor"></button>
        </form>

        <div class="search-close">
            <a href="#" class="js-site-search-toggle"></a>
        </div>
    </div>

    <div class="popular-searches">
        <div class="container">
            <div class="headline">
                <h4>Popular Searches</h4>
            </div>

            <ul>

                <?php if(have_rows('popular_searches', 'options')): while(have_rows('popular_searches', 'options')): the_row(); ?>
                    <?php 
                        $link = get_sub_field('link');
                        if( $link ): 
                        $link_url = $link['url'];
                        $link_title = $link['title'];
                        $link_target = $link['target'] ? $link['target'] : '_self';
                    ?>

                        <li>
                            <a href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
                        </li>

                    <?php endif; ?>
                <?php endwhile; endif; ?>

            </ul>            
        </div>
    </div>

</div>

<div class="blur-background js-site-search-toggle"></div>