<div class="marquee">
    <div class="marquee-slider">

        <?php if(have_rows('marquee', 'options')): while(have_rows('marquee', 'options')): the_row(); ?>
            <div class="message">
                <?php echo get_sub_field('text'); ?>
            </div>
        <?php endwhile; endif; ?>

    </div>
</div>