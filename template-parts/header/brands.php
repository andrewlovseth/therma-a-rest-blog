<div class="brands">
    <?php if(have_rows('network_navigation', 'options')): while(have_rows('network_navigation', 'options')): the_row(); ?>

        <a class="network-link <?php echo sanitize_title_with_dashes(get_sub_field('name')); ?>" href="<?php echo get_sub_field('link'); ?>">
            <img src="<?php $image = get_sub_field('logo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
        </a>

    <?php endwhile; endif; ?>
</div>