<?php $logo = get_field('site_logo', 'options'); if( $logo ): ?>
    <div class="logo">
        <a href="https://thermarest.com/">
            <?php echo wp_get_attachment_image($logo['ID'], 'full'); ?>
        </a>
    </div>
<?php endif; ?>