<?php
    // Country Code
    $ip_obj = geoip_detect2_get_info_from_ip($_SERVER['REMOTE_ADDR']);
    $country = $ip_obj->raw['country']['iso_code'];
    if($country !== NULL) {
        $country_code = $country;
    } else {
        $country_code = "US";
    }    

    // Currency Code
    if(ICL_LANGUAGE_CODE == 'fr' || ICL_LANGUAGE_CODE == 'de') {
        $currency_code = 'EUR';
    } else {
        $currency_code = 'USD';
    }
?>


<script>
    window.dataLayer = window.dataLayer || [];
    window.dataLayer.push({
    'event': 'dataLayer-loaded',
    'country': '<?php echo $country_code; ?>',
    'currencyCode': '<?php echo $currency_code; ?>',
    'brand': 'tar',
    'customerType': 'unregistered',
    'pageType': 'blog'
    });
</script>