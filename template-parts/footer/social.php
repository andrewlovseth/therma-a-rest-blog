<div class="col social">
    <div class="header">
        <h4><?php echo get_field('social_header', 'options'); ?></h4>
    </div>

    <ul>
        <?php if(have_rows('social', 'options')): while(have_rows('social', 'options')): the_row(); ?>

            <li><a class="<?php echo sanitize_title_with_dashes(get_sub_field('network')); ?>" href="<?php echo get_sub_field('link'); ?>" target="_blank"><img src="<?php echo get_sub_field('icon'); ?>" alt="<?php echo get_sub_field('network'); ?>" /></a></li>

        <?php endwhile; endif; ?>
    </ul>
</div>