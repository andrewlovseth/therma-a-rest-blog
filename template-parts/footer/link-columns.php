<?php if(have_rows('footer_link_columns', 'options')): while(have_rows('footer_link_columns', 'options')) : the_row(); ?>

    <?php if( get_row_layout() == 'column' ): ?>

        <div class="col">
            <div class="header">
                <h4><?php echo get_sub_field('column_header'); ?></h4>
            </div>

            <ul>
                <?php if(have_rows('links')): while(have_rows('links')): the_row(); ?>
                    <?php 
                        $link = get_sub_field('link');
                        if( $link ): 
                        $link_url = $link['url'];
                        $link_title = $link['title'];
                        $link_target = $link['target'] ? $link['target'] : '_self';
                    ?>

                        <li><a class="<?php echo sanitize_title_with_dashes($link_title); ?>" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a></li>

                    <?php endif; ?>
                <?php endwhile; endif; ?>
            </ul>
            
        </div>

    <?php endif; ?>

<?php endwhile; endif; ?>