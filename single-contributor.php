<?php get_header(); ?>

	<?php get_template_part('templates/single-contributor/bio'); ?>

	<?php get_template_part('templates/single-contributor/posts'); ?>

<?php get_footer(); ?>