<?php get_header(); ?>


<?php
    $paged = (get_query_var('page')) ? get_query_var('page') : 1;
    $s = get_search_query();

    $args = array(
        'posts_per_page' => 12,
        'paged' => $paged,
        's' => $s
    );

    $data = new WP_Query($args);

?>

	<?php if ( $data->have_posts() ): ?>

		<section id="posts">
			<div class="wrapper">

				<h2 class="section-header">Search <strong>/ <?php echo get_search_query(); ?></strong></h2>

				<section class="posts-wrapper archive">
				
					<?php  while($data->have_posts())  : $data->the_post(); ?>

						<article class="post">
							<?php get_template_part('template-parts/global/article'); ?>
						</article>

					<?php endwhile; ?>

				</section>


				<?php
					$total_pages = $data->max_num_pages;
					if ($total_pages > 1):
				?>
					<section id="pagination">
						<nav class="navigation pagination">
							<h2 class="screen-reader-text">Page</h2>
							
							<div class="nav-links">
								<?php
									$current_page = max(1, get_query_var('page'));
						
									echo paginate_links(array(
										'base' => @add_query_arg('page','%#%'),
										'format' => '?page=%#%',
										'current' => $current_page,
										'total' => $total_pages,
										'mid_size' => 3,
										'prev_text' => '&lt;',
										'next_text' => '&gt;'
									));
								?> 
							</div>
						</nav>
					</section>
				<?php endif; ?>

			</div>
		</section>

	<?php else: ?>

		<section id="posts" class="no-results">
			<div class="wrapper">

				<h2 class="section-header">Search <strong>/ <?php echo get_search_query(); ?></strong></h2>

				<section class="posts-wrapper archive">
				
						<article class="post">
							<h3>Sorry, but nothing matched your search terms. Please try again with some different keywords.</h3>
						</article>


				</section>

			</div>
		</section>

	<?php endif; ?>

<?php get_footer(); ?>