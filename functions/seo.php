<?php



// CUSTOM SEO IMAGES
function remove_default_seo_images($input) {
    if (is_single()) {
      add_filter('wpseo_opengraph_image' , '__return_false' );
      add_filter('wpseo_twitter_image' , '__return_false' );
    }
}

add_action('wp_head', 'remove_default_seo_images', 1);

/*function add_custom_seo_images($input) {
    if (is_single()) {
      global $post;
      $og_image = get_field('featured_image', $post->ID);
      if ($og_image) {
          $image_url = $og_image['sizes']['medium'];
          $image_width = $og_image['sizes']['medium-width'];
          $image_height = $og_image['sizes']['medium-height'];
          $GLOBALS['wpseo_og']->image($image_url);
      ?>
<meta property="og:image" content="<?php echo $image_url; ?>" />
<meta property="og:image:width" content="<?php echo $image_width; ?>" />
<meta property="og:image:height" content="<?php echo $image_height; ?>" />
<meta name="twitter:image" content="<?php echo $image_url; ?>" />

      <?php
      }
    }
}

add_action('wp_head', 'add_custom_seo_images', 7);
*/


