<?php

// Register and noConflict jQuery 3.2.1
wp_register_script( 'jquery.3.2.1', 'https://code.jquery.com/jquery-3.2.1.min.js' );
wp_add_inline_script( 'jquery.3.2.1', 'var jQuery = $.noConflict(true);' );

// Enqueue custom styles and scripts
function enqueue_styles_and_scripts() {


    $uri = get_stylesheet_directory_uri();
    $dir = get_stylesheet_directory();

    $script_last_updated_at = filemtime($dir . '/js/site.js');
    $style_last_updated_at = filemtime($dir . '/style.css');

    // Add style.css
    wp_enqueue_style('adobe-fonts', 'https://use.typekit.net/rpx1vhj.css');
    wp_enqueue_style('style', get_stylesheet_directory_uri() . '/style.css' , '', $style_last_updated_at);

    // Add plugins.js & site.js (with jQuery dependency)
    wp_enqueue_script('plugins', get_stylesheet_directory_uri() . '/js/plugins.js', array( 'jquery.3.2.1' ), $script_last_updated_at, true);
    wp_enqueue_script('site', get_stylesheet_directory_uri() . '/js/site.js', array( 'jquery.3.2.1' ), $script_last_updated_at, true);
}
add_action('wp_enqueue_scripts', 'enqueue_styles_and_scripts');