<?php

function archives_posts( $query ) {

    if ( is_admin() || ! $query->is_main_query() )
        return;

    if ( is_archive() || is_search() ) {
        
        $query->set( 'posts_per_page', 12 );
        $query->set( 'post_type', 'post' );

        return;
    }

  
}
add_action( 'pre_get_posts', 'archives_posts', 1 );