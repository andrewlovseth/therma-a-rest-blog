<?php



function my_relationship_query( $args, $field, $post_id ) {

    $args['orderby'] = 'date';
    $args['order'] = 'DESC';

    return $args;
}


// filter for every field
add_filter('acf/fields/relationship/query', 'my_relationship_query', 10, 3);


add_filter( 'acf/fields/post_object/query', 'change_posts_order' );
function change_posts_order( $args ) {
	$args['orderby'] = 'date';
	$args['order'] = 'DESC';
	return $args;
}



if( function_exists('acf_add_options_page') ) {
	acf_add_options_page();
}

if( function_exists('acf_add_options_sub_page') ) {

	acf_add_options_sub_page('Header');
	acf_add_options_sub_page('Mobile Navigation');
	acf_add_options_sub_page('Footer');
	acf_add_options_sub_page('Homepage');

}

function my_acf_admin_head() {
	?>
	<style type="text/css">

		.acf-relationship .list {
			height: 400px;
		}

	</style>

	<?php
}

add_action('acf/input/admin_head', 'my_acf_admin_head');


/**
 * ACF / WPML Options
 * Return the value of an Options field from WPML's default language
 */

function barebones_acf_set_language() {
  return acf_get_setting('default_language');
}
 
function get_global_option($name) {
    add_filter('acf/settings/current_language', 'barebones_acf_set_language', 100);
    $option = get_field($name, 'option');
    remove_filter('acf/settings/current_language', 'barebones_acf_set_language', 100);
    return $option;
}