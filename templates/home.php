<?php 

/*

    Template Name: Home

*/

get_header(); ?>

	<?php get_template_part('templates/home/featured-posts'); ?>

	<?php get_template_part('templates/home/latest-posts'); ?>

	<?php get_template_part('templates/home/pagination'); ?>


	<?php get_template_part('templates/home/explore'); ?>


	<?php get_template_part('templates/home/instagram'); ?>

<?php get_footer(); ?>