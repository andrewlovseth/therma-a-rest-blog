<section id="list">
    <div class="wrapper">

        <h2 class="section-header">Our Contributors</h2>

        <div class="list-wrapper">

            <?php
                $args = array(
                    'post_type' => 'contributor',
                    'posts_per_page' => 200,
                    'order' => 'ASC',
                    'orderby' => 'title'
                );
                $query = new WP_Query( $args );
                if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>

                <div class="item">

                    <div class="photo">
                        <a href="<?php the_permalink(); ?>">
                            <div class="content">
                                <img src="<?php $image = get_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                            </div>
                        </a>
                    </div>
                    <div class="info">
                        <h5><?php echo get_field('title'); ?></h5>
                        <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                        <?php echo get_field('short_bio'); ?>

                        <div class="links">
                            <a href="<?php the_permalink(); ?>">Read full bio</a>

                            <div class="social">

                                <?php if(get_field('twitter')): ?>
                                    <a href="<?php echo get_field('twitter'); ?>" rel="external"><img src="<?php echo bloginfo('template_directory'); ?>/images/social-twitter.png" alt="Twitter" /></a>
                                <?php endif; ?>

                                <?php if(get_field('facebook')): ?>
                                    <a href="<?php echo get_field('facebook'); ?>" rel="external"><img src="<?php echo bloginfo('template_directory'); ?>/images/social-facebook.png" alt="Twitter" /></a>
                                <?php endif; ?>

                                <?php if(get_field('instagram')): ?>
                                    <a href="<?php echo get_field('instagram'); ?>" rel="external"><img src="<?php echo bloginfo('template_directory'); ?>/images/social-instagram.png" alt="Twitter" /></a>
                                <?php endif; ?>
                            </div>
                            
                        </div>

                    </div>

                </div>

            <?php endwhile; endif; wp_reset_postdata(); ?>

        </div>

    </div>
</section>