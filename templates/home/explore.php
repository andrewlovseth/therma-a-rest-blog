<section id="explore">
    <div class="wrapper">

        <h2 class="section-header">Explore by Topic</h2>

        <section class="tag-wrapper">
            <?php $terms = get_field('explore_by_topic', 'options'); if( $terms ): ?>

                <?php foreach( $terms as $term ): ?>
                    <div class="tag">
                        <a href="<?php echo get_term_link( $term ); ?>"><?php echo $term->name; ?></a>
                    </div>
                <?php endforeach; ?>

            <?php endif; ?>
        </section>

    </div>
</section>