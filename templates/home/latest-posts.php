<section id="posts">
    <div class="wrapper">

        <h2 class="section-header">Latest Posts</h2>

        <?php $exclude_ids = array(); if(have_rows('featured_posts', 'options')): while(have_rows('featured_posts', 'options')): the_row(); ?>

            <?php $post_object = get_sub_field('post'); $exclude_ids[] = $post_object->ID; ?>
                            
        <?php endwhile; endif; ?>

        <section class="posts-wrapper">

            <?php
                $counter = 1;
                $args = array(
                    'post_type' => 'post',
                    'posts_per_page' => 7,
                    'post__not_in' => $exclude_ids,
                    'suppress_filters' => false
                );
                $query = new WP_Query( $args );
                if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>

                    
                    <?php if($counter == 3): ?>

                        <article class="post highlight recent recent-<?php echo $counter; ?>">
                            <?php get_template_part('template-parts/global/article'); ?>
                        </article>

                    <?php else: ?>

                        <article class="post recent recent-<?php echo $counter; ?>">
                            <?php get_template_part('template-parts/global/article'); ?>
                        </article>

                    <?php endif; ?>

            <?php $counter++; endwhile; endif; wp_reset_postdata(); ?>

        </section>
        
        <?php get_template_part('template-parts/home/pagination'); ?>

    </div>
</section>