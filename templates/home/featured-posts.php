<section id="featured">
    <div class="wrapper">

        <section class="posts-wrapper">

            <?php $counter = 1; if(have_rows('featured_posts', 'options')): while(have_rows('featured_posts', 'options')): the_row(); ?>

                <?php $post_object = get_sub_field('post'); if( $post_object ): $post = $post_object; setup_postdata( $post ); ?>
        
                    <?php if($counter == 1): ?>

                        <article class="post highlight featured-<?php echo $counter; ?>">
                            <?php get_template_part('template-parts/global/featured-article'); ?>
                        </article>

                    <?php else: ?>

                        <article class="post featured-<?php echo $counter; ?>">
                            <?php get_template_part('template-parts/global/article'); ?>
                        </article>

                    <?php endif; ?>

                <?php wp_reset_postdata(); endif; ?>
            
            <?php $counter++; endwhile; endif; ?>

        </section>

    </div>
</section>