<?php if ( function_exists( 'sb_instagram_feed_init' ) || function_exists( 'display_instagram' ) ): ?>

    <section id="instagram">
        <div class="wrapper">

            <?php echo do_shortcode('[instagram-feed]'); ?>

            <div id="handle">
                <a href="https://www.instagram.com/thermarest/" rel="external">@thermarest</a>
            </div>
            
        </div>
    </section>

<?php endif; ?>