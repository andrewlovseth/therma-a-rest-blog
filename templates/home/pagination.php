<section id="pagination">
    <nav class="navigation pagination" role="navigation">

        <h2 class="screen-reader-text">Page</h2>
        
        <div class="nav-links">
            <span class="page-numbers current">1</span>
            <a class="page-numbers" href="<?php echo site_url('/archives/page/2/'); ?>">2</a>
            <a class="page-numbers" href="<?php echo site_url('/archives/page/3/'); ?>">3</a>
            <a class="page-numbers" href="<?php echo site_url('/archives/page/4/'); ?>">4</a>
            <span class="page-numbers dots">…</span>
            <a class="next page-numbers" href="<?php echo site_url('/archives/page/2/'); ?>" aria-label="Next page of posts">&gt;</a>									
        </div>
    
    </nav>
</section>
