<section id="bio">
    <div class="wrapper">

        <h2 class="section-header"><a href="<?php echo site_url('/contributors/'); ?>">Our Contributors</a> <strong>/ <?php the_title(); ?></strong></h2>

        <div class="photo">
            <div class="content">
                <img src="<?php $image = get_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
            </div>				
        </div>

        <div class="info">
            <h5><?php echo get_field('title'); ?></h5>
            <h1><?php the_title(); ?></h1>
            <?php echo get_field('bio'); ?>

            <div class="links">
                <div class="social">

                    <?php if(get_field('twitter')): ?>
                        <a href="<?php echo get_field('twitter'); ?>" rel="external"><img src="<?php echo bloginfo('template_directory'); ?>/images/social-twitter.png" alt="Twitter" /></a>
                    <?php endif; ?>

                    <?php if(get_field('facebook')): ?>
                        <a href="<?php echo get_field('facebook'); ?>" rel="external"><img src="<?php echo bloginfo('template_directory'); ?>/images/social-facebook.png" alt="Twitter" /></a>
                    <?php endif; ?>

                    <?php if(get_field('instagram')): ?>
                        <a href="<?php echo get_field('instagram'); ?>" rel="external"><img src="<?php echo bloginfo('template_directory'); ?>/images/social-instagram.png" alt="Twitter" /></a>
                    <?php endif; ?>
                </div>					
            </div>

        </div>

    </div>
</section>