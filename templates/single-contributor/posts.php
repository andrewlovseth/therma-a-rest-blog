<section id="posts">
		<div class="wrapper">

			<h2 class="section-header">Recent posts by <?php the_title(); ?></h2>

			<section class="posts-wrapper profile">

				<?php 
					$relatedPosts = get_posts(array(
						'post_type' => 'post',
						'posts_per_page' => 100,
						'meta_query' => array(
							array(
								'key' => 'contributor', // name of custom field
								'value' => '"' . get_the_ID() . '"', // matches exaclty "123", not just 123. This prevents a match for "1234"
								'compare' => 'LIKE'
							)
						)
					));
				?>

				<?php if( $relatedPosts ): ?>
					<?php foreach( $relatedPosts as $relatedPost ): ?>

						<article class="post profile">
							<a href="<?php echo get_permalink( $relatedPost->ID ); ?>" class="photo">
								<div class="content">
									<img src="<?php $image = get_field('featured_image', $relatedPost->ID); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
								</div>
							</a>

							<div class="info">
								<?php 
								$categories = get_the_category($relatedPost->ID);
								if ( ! empty( $categories ) ) : ?>
									<h4 class="category"><a href="<?php echo esc_url( get_category_link( $categories[0]->term_id ) ); ?>"><?php echo esc_html( $categories[0]->name ); ?></a></h4>
								<?php endif; ?>

								<h2 class="title"><a href="<?php echo get_permalink( $relatedPost->ID ); ?>"><?php echo get_the_title( $relatedPost->ID ); ?></a></h2>

								<h5>
									<?php $num_comments = get_comments_number($relatedPost->ID);

										if ( $num_comments == 0 ) {
											$comments = __('0 Comments');
										} elseif ( $num_comments > 1 ) {
											$comments = $num_comments . __(' Comments');
										} else {
											$comments = __('1 Comment');
										}

										echo $comments;
									?>
								</h5>

							</div>
						</article>

					<?php endforeach; ?>
				<?php endif; ?>

			</section>

		</div>
	</section>