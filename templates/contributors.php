<?php 

/*

    Template Name: Contributors

*/

get_header(); ?>

	<?php get_template_part('templates/contributors/list'); ?>

	<?php get_template_part('template-parts/global/popular-post'); ?>

<?php get_footer(); ?>