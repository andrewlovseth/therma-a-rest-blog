	<footer class="site-footer">
		<div class="columns">

			<?php get_template_part('template-parts/footer/link-columns'); ?>

			<?php get_template_part('template-parts/footer/social'); ?>

			<div class="col lang-switcher">
				<?php do_action('wpml_add_language_selector'); ?>
			</div>
			
		</div>
	</footer>

	<?php get_template_part('template-parts/footer/corporate-footer'); ?>

	<?php wp_footer(); ?>

</body>
</html>