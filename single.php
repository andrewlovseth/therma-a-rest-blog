<?php get_header(); ?>

	<article>

		<?php if(get_field('featured_image')): ?>
	    	<div class="featured-image">
	    		<div class="wrapper">

	    			<div class="photo">
	    				<div class="content">
		    				<img src="<?php $image = get_field('featured_image'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />	    					
	    				</div>
	    			</div>

	    		</div>
	    	</div>
	    <?php endif; ?>

		<div class="article-header">
	    	<div class="wrapper">

				<h4 class="category"><?php the_category( ' ' ); ?></h4>
				<h1 class="title"><?php the_title(); ?></h1>
				<h3 class="byline">

					<?php if(get_field('one_time_contributor') == true): ?>

						<?php echo get_field('contributor_name'); ?> | 

					<?php else: ?>

						<?php $contributors = get_field('contributor'); if( $contributors ): ?>

							<?php foreach( $contributors as $contributor): ?>
								<a class="contributor" href="<?php echo get_permalink($contributor->ID); ?>"><?php echo get_the_title($contributor->ID); ?></a> | 
							<?php endforeach; ?>

						<?php endif; ?>
					
					<?php endif; ?>

					<?php the_time('F j, Y'); ?>
				</h3>

			</div>
		</div>

		<div class="article-body">
	    	<div class="wrapper">

				<?php the_content(); ?>

			</div>
	    </div>

	    <div class="article-footer">
	    	<div class="wrapper">

	    		<?php get_template_part('template-parts/article-footer/tags'); ?>

	    		<?php get_template_part('template-parts/article-footer/products'); ?>

	    		<?php get_template_part('template-parts/article-footer/contributor'); ?>

	    		<?php get_template_part('template-parts/article-footer/share'); ?>

	    		<?php get_template_part('template-parts/article-footer/comments'); ?>

	    	</div>
	    </div>

	</article>

	<?php get_template_part('template-parts/article-footer/related-posts'); ?>

<?php get_footer(); ?>