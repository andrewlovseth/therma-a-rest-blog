(function ($, window, document, undefined) {
    function closeMobileNav() {
        let target = 'main';
        $('.mobile-navigation-container > ul').remove();
        $('.screens-container')
            .find(`[data-id='${target}']`)
            .clone(true)
            .appendTo('.mobile-navigation-container');
    }

    function getOffset(el) {
        let rect = el.getBoundingClientRect();

        $(window).resize(function () {
            let rect = el.getBoundingClientRect();
        });
        return rect.left + window.scrollX;
    }

    function firstLevelXPosition() {
        $('.first-level').each(function () {
            let x = getOffset(this);
            let menu = $(this).find('.dropdown-menu');

            $(menu).css('left', -x);
        });
    }

    $(document).ready(function () {
        // rel="external"
        $('a[rel="external"]').click(function () {
            window.open($(this).attr('href'));
            return false;
        });

        // Header - Announcement Overlay
        $('.announcement-trigger').on('click', function () {
            $('article.announcement-overlay').addClass('active');

            return false;
        });

        $('.announcement-close').on('click', function () {
            $('article.announcement-overlay').removeClass('active');

            return false;
        });

        // Header - Hamburger Menu
        $('.js-hamburger-btn').on('click', function () {
            $(this).toggleClass('show');
            $('body').toggleClass('mobile-nav-show');
            closeMobileNav();
            return false;
        });

        // Initial mobile screen
        $('.screens-container')
            .find('[data-id="main"]')
            .clone(true)
            .appendTo('.mobile-navigation-container');

        // Mobile Nav - Forward Link
        $('.mobile-navigation .forward a, .mobile-navigation .back a').on(
            'click',
            function () {
                let target = $(this).data('target');
                $('.mobile-navigation-container > ul').remove();
                $('.screens-container')
                    .find(`[data-id='${target}']`)
                    .clone(true)
                    .appendTo('.mobile-navigation-container');
                return false;
            }
        );

        $('.js-site-search-toggle').on('click', function () {
            $('body').toggleClass('search-overlay-active');

            return false;
        });

        // Header - Search Submit
        $('.site-search .search-form').on('submit', function () {
            if ($('.site-search .search-form .search-input').val()) {
                var searchTerm = $('.search-form .search-input').val(),
                    searchQuery =
                        'https://www.thermarest.com/search?q=' +
                        encodeURIComponent(searchTerm);
                window.location = searchQuery;
            }
            return false;
        });

        firstLevelXPosition();

        $(window).resize(function () {
            firstLevelXPosition();
        });

        $('.first-level > .main-link').hover(function () {
            $('.main-link').removeClass('show');
            $('.dropdown-menu').removeClass('show');

            let menu = $(this).siblings('.dropdown-menu');

            $(this).addClass('show');
            $(menu).addClass('show');
        });

        $('.desktop-nav').mouseleave(function () {
            $('.main-link').removeClass('show');
            $('.dropdown-menu').removeClass('show');
        });

        // Search Close Trigger
        $(document).on('click', '.search-trigger-closed', function (e) {
            $('.search-trigger').toggleClass(
                'search-trigger-closed search-trigger-open'
            );
            $('.search-btn').toggleClass('closed open');

            return false;
        });

        // Search Open Trigger
        $(document).on('click', '.search-trigger-open', function (e) {
            var search_value = $('.blog-search .search-field').val();

            if (search_value.length > 0) {
                var searchQuery =
                    'https://www.thermarest.com/blog/?s=' +
                    encodeURIComponent(search_value);
                window.location = searchQuery;
            } else {
                $('.search-trigger').toggleClass(
                    'search-trigger-closed search-trigger-open'
                );
                $('.search-btn').toggleClass('closed open');
            }

            return false;
        });

        // Header - Hover show dropdowns
        $('.menu-item.dropdown')
            .on('mouseenter', function () {
                $(this).find('.dropdown-nav').addClass('show');
            })
            .on('mouseleave', function () {
                $(this).find('.dropdown-nav').removeClass('show');
            });

        $('.menu-item.dropdown a.dropdown-link').on('click', function () {
            return false;
        });

        // Header - Network Nav Toggle
        $('.network-trigger').on('click', function () {
            $('nav.utility .network').toggleClass('show');

            return false;
        });

        $('.link-trigger').on('click', function () {
            var view = $('.page-link-view'),
                link = $(this).data('link'),
                parent = $(this).closest('.page-link').toggleClass('active');

            $(view).html(link);

            return false;
        });

        // Blog Nav - Toggle Trigger
        $('.blog-nav-trigger').click(function () {
            $('.blog-nav').toggleClass('open');

            return false;
        });

        $('#posts.related .posts-wrapper').slick({
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            mobileFirst: true,
            responsive: [
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2,
                        infinite: true,
                    },
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                    },
                },
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 4,
                        slidesToScroll: 4,
                    },
                },
            ],
        });

        $('form#gform_1')
            .on('change', function () {
                var validated = false;
                var email_field = $(
                    '.gfield_contains_required #input_1_2:not(.LV_invalid_field)'
                );
                var country_field = $('.gfield_contains_required #input_1_3');
                var submit_field = $('form#gform_1 input[type="submit"]');

                if (
                    $(email_field).val() &&
                    $(country_field)[0].selectedIndex > 0
                ) {
                    validated = true;
                }

                $(submit_field)
                    .addClass('disabled')
                    .attr('disabled', 'disabled');

                if (validated == true) {
                    $(submit_field)
                        .removeClass('disabled')
                        .removeAttr('disabled');
                }
            })
            .trigger('change');

        $('.marquee-slider').slick({});
    });

    $(document).keyup(function (e) {
        if (e.keyCode == 27) {
            $('.js-hamburger-btn').removeClass('show');
            $('body').removeClass('mobile-nav-show search-overlay-active');
            closeMobileNav();
        }
    });

    window.addEventListener('resize', function (e) {
        let windowWidth = window.innerWidth;
        if (windowWidth >= 992) {
            $('.js-hamburger-btn').removeClass('show');
            $('body').removeClass('mobile-nav-show search-overlay-active');
            closeMobileNav();
        }
    });
})(jQuery, window, document);
