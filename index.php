<?php get_header(); ?>

	<?php if ( have_posts() ): ?>

		<section id="posts">
			<div class="wrapper">

				<h2 class="section-header">Archives</h2>

				<section class="posts-wrapper archive">
				
					<?php while ( have_posts() ) : the_post(); ?>

						<article class="post">
							<?php get_template_part('template-parts/global/article'); ?>
						</article>

					<?php endwhile; ?>

				</section>

				<?php get_template_part('template-parts/global/pagination'); ?>

			</div>
		</section>

	<?php endif; ?>

<?php get_footer(); ?>