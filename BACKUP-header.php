<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<?php get_template_part('template-parts/header/ga-data'); ?>

	<?php echo get_field('head_html', 'options'); ?>

	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<?php echo get_field('body_html', 'options'); ?>

	<header class="site-header">

		<?php get_template_part('template-parts/header/announcement-banner'); ?>

		<?php get_template_part('template-parts/header/utility-navigation'); ?>

		<?php get_template_part('template-parts/header/main-navigation'); ?>
	
	</header>

	<?php get_template_part('template-parts/header/blog-nav'); ?>